import time
import os

# --- interface ---

def setup():
	pass

def destroy():
	pass

def showStart():
	print('Comença el joc!')
	time.sleep(1)
	os.system('clear')

def showChallenge(challenge):
	for color in challenge:
		print(color)
		time.sleep(1)
	os.system('clear')

def prompt():
	answer = input('Següent color: ')
	os.system('clear')
	return answer

def showError():
	print('T\'has equivocat!')
	print('Game over')



# --- private ---

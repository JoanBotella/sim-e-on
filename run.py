#! /usr/bin/python3

# --- Setup ---

import sys
import colors
import random

if len(sys.argv) == 2 and sys.argv[1] == 'raspi':
	import raspi_driver as driver
else:
	import screen_driver as driver

possiblecolors = (colors.RED, colors.GREEN, colors.BLUE, colors.YELLOW)
level = 1



# --- Start ---

driver.setup()
driver.showStart()

try:
	while True:

		challenge = random.choices(possiblecolors, k = level)

		driver.showChallenge(challenge)

		for index in range(0,len(challenge)):
			answer = driver.prompt()

			if answer != challenge[index]:
				driver.showError()
				sys.exit()

		level += 1

except KeyboardInterrupt:
	driver.destroy()

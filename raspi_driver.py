import RPi.GPIO as GPIO
import colors
import time

PIN_LED_RED = 17
PIN_LED_GREEN = 22
PIN_LED_BLUE = 19
PIN_LED_YELLOW = 26

PIN_BUTTON_RED = 18
PIN_BUTTON_GREEN = 23
PIN_BUTTON_BLUE = 20
PIN_BUTTON_YELLOW = 21



# --- interface ---

def setup():
	GPIO.setmode(GPIO.BCM)

	GPIO.setup(PIN_LED_RED, GPIO.OUT)
	GPIO.setup(PIN_LED_GREEN, GPIO.OUT)
	GPIO.setup(PIN_LED_BLUE, GPIO.OUT)
	GPIO.setup(PIN_LED_YELLOW, GPIO.OUT)

	GPIO.setup(PIN_BUTTON_RED, GPIO.IN, pull_up_down = GPIO.PUD_UP)
	GPIO.setup(PIN_BUTTON_GREEN, GPIO.IN, pull_up_down = GPIO.PUD_UP)
	GPIO.setup(PIN_BUTTON_BLUE, GPIO.IN, pull_up_down = GPIO.PUD_UP)
	GPIO.setup(PIN_BUTTON_YELLOW, GPIO.IN, pull_up_down = GPIO.PUD_UP)

def destroy():
	GPIO.cleanup()

def showStart():
	for index in range(2):
		switchOnLed(colors.RED)
		time.sleep(0.25)
		switchOffLed(colors.RED)

		switchOnLed(colors.GREEN)
		time.sleep(0.25)
		switchOffLed(colors.GREEN)

		switchOnLed(colors.BLUE)
		time.sleep(0.25)
		switchOffLed(colors.BLUE)

		switchOnLed(colors.YELLOW)
		time.sleep(0.25)
		switchOffLed(colors.YELLOW)

	time.sleep(1)

def showChallenge(challenge):
	for color in challenge:
		switchOnLed(color)
		time.sleep(1)
		switchOffLed(color)
		time.sleep(0.5)

def showError():
	for index in range(2):
		switchOnLed(colors.RED)
		switchOnLed(colors.GREEN)
		switchOnLed(colors.BLUE)
		switchOnLed(colors.YELLOW)

		time.sleep(0.5)

		switchOffLed(colors.RED)
		switchOffLed(colors.GREEN)
		switchOffLed(colors.BLUE)
		switchOffLed(colors.YELLOW)

		time.sleep(0.5)

def prompt():
	color = ''

	while True:
		if GPIO.input(PIN_BUTTON_RED) == GPIO.LOW:
			color = colors.RED
			print('Botó ' + color + ' pressionat')

		elif GPIO.input(PIN_BUTTON_GREEN) == GPIO.LOW:
			color = colors.GREEN
			print('Botó ' + color + ' pressionat')

		elif GPIO.input(PIN_BUTTON_BLUE) == GPIO.LOW:
			color = colors.BLUE
			print('Botó ' + color + ' pressionat')

		elif GPIO.input(PIN_BUTTON_YELLOW) == GPIO.LOW:
			color = colors.YELLOW
			print('Botó ' + color + ' pressionat')

		elif color != '':
			print('Retornant color ' + color)
			return color



# --- private ---

def switchOnLed(color):
	print('Encenent LED ' + color)

	ledPin = getLedPinByColor(color)
	GPIO.output(ledPin, GPIO.HIGH)

def getLedPinByColor(color):
	if color == colors.RED:
		return PIN_LED_RED

	elif color == colors.GREEN:
		return PIN_LED_GREEN

	elif color == colors.BLUE:
		return PIN_LED_BLUE

	elif color == colors.YELLOW:
		return PIN_LED_YELLOW

def switchOffLed(color):
	print('Apagant LED ' + color)

	ledPin = getLedPinByColor(color)
	GPIO.output(ledPin, GPIO.LOW)

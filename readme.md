# sim-e-on

A simple memory game for learning the basics of programming and for building a Raspberry Pi circuit with LEDs and buttons.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Author

Joan Botella Vinaches <https://joanbotella.com>
